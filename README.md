# postman-api-fuzzing-example

This project is an example of API Fuzzing using a Postman Collection. The Postman Collection
provides the API operations to test. The provided sample data will be used when calling the API operations.

It's assumed that during the `deploy` stage the target API is deployed into a staging environment that is suitable to be tested. Testing should never occur against a production deployment.

The testing results are available via the `Test` tab of the pipeline.
